# Installation

In `src/environments/environment.ts`, change the path of "apiUrl" 

Basic Setup

```sh
$ sudo npm install -g @ionic/cli
$ npm install
```

## Run App in Browser

```sh
$ ionic serve --lab
```
## Run App in Android

```sh
$ ionic build
$ npx cap add android
$ npx cap sync
```