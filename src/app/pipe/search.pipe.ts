import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchfilter'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!value)return null;
    if(!args)return value;


    return value.filter(function(item){
      console.log("item",item);
      return item.project.id ==  args
        // return JSON.stringify(item).toLowerCase().includes(args);
    });
  }

}
