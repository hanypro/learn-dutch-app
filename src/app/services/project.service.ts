import { Injectable } from '@angular/core';
import {IProjectInterface} from '../types/project.interface'; 
import { Observable } from 'rxjs' ;
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  constructor(private http: HttpClient) { }

  //Request all Projects from the API
  getProjects(): Observable<IProjectInterface[]> {
    return this.http.get<IProjectInterface[]>(`${environment.apiUrl}api/projects`).pipe(
      map((project: IProjectInterface[]) => {
        return project.map(project => ({
          id: project.id,
          name: project.name,
        }))
      })
    )
  }
}
