import { Injectable } from '@angular/core';
import { Observable,throwError  } from 'rxjs' ;
import {ITaskInterface} from '../types/task.interface'; 
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { environment } from '../../environments/environment';
import {  catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  //Request all tasks from the API
  getTasks(): Observable<ITaskInterface[]> {
    return this.http.get<ITaskInterface[]>(`${environment.apiUrl}api/tasks`).pipe(
      catchError(this.handleError) 
    );
  }

  //Add the task 
  addTask(task:ITaskInterface): Observable<ITaskInterface> {
    return this.http.post<ITaskInterface>(`${environment.apiUrl}api/tasks`, task)
  }

  //Remove the task by using task ID
  removeTask(id:string): Observable<{}> {
    return this.http.delete(`${environment.apiUrl}api/tasks/${id}`)
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

}
