import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {ITaskInterface} from '../../types/task.interface'; 
import {TaskService} from '../../services/task.service';
import  {ProjectService} from '../../services/project.service';
import {IProjectInterface} from '../../types/project.interface'; 
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  
  constructor(private taskService:TaskService ,
              private projectService:ProjectService,
              public alertController: AlertController
              ) { }
  tasks:ITaskInterface[];
  projects:  IProjectInterface[];
  searchbar: boolean = false;
  searchText :'';
  ngOnInit() {
    //Request all Projects 
    this.getProjects();
  }
  
  ionViewWillEnter() {
    this.taskService.getTasks().subscribe((tasks:ITaskInterface[])=>{
      this.tasks =  tasks;
      console.log("tasks",tasks);
  })
  }
  //Request all Projects 
  getProjects(){
    this.projectService.getProjects().subscribe((projects:IProjectInterface[])=>{
      this.projects =  projects;
      console.log("projects",projects);
    })
  }
  //Remove a task  
  removeTask(id: string): void {
    this.presentAlertConfirm(id);
  }

  async presentAlertConfirm(id: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: ' <strong>Are you sure to delete this task?</strong>!!!',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Yes',
          handler: () => {
             this.taskService.removeTask(id).subscribe((data) => {
               console.log("data",data);
              this.tasks = this.tasks.filter(task => task.id !== id);
            })
          }
        }
      ]
    });
    await alert.present();
  }
}
