import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import {  NavController } from "@ionic/angular";
import { ToastController } from '@ionic/angular';
import { ProjectService } from 'src/app/services/project.service';
import { TaskService } from 'src/app/services/task.service';
import { IProjectInterface } from 'src/app/types/project.interface';
import { ITaskInterface } from 'src/app/types/task.interface';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.page.html',
  styleUrls: ['./add-task.page.scss'],
})
export class AddTaskPage implements OnInit {

  constructor(
    private projectService:ProjectService,
    public formBuilder: FormBuilder,
    private taskService:TaskService,
    private navCtrl: NavController,
    public toastController: ToastController
    ) { }

  projects : IProjectInterface[];
  taskForm: FormGroup;
  loading = false;
  async   ngOnInit() {
    this.projectService.getProjects().subscribe((projects:IProjectInterface[])=>{
      this.projects =  projects;
      console.log("projects",projects);
    })
    this.setForm();
   
  }
  async showToast(mesage:string)  {
    const toast = await this.toastController.create({
      message: mesage,
      duration: 2000
    });
    toast.present();
  }
  setForm(): void {
    this.taskForm = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      project: new FormControl('', Validators.required),
    });
  } 
  submit(): void {
    this.loading = true;
    this.addTask();
  }
  addTask(): void {
    let task = this.taskForm.value as ITaskInterface;
    this.taskService.addTask(task).subscribe(newTask =>{
      this.showToast("Your Task has been added.");
      this.navCtrl.back();
    });
  }
 



}