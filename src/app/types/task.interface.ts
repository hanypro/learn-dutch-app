import {IProjectInterface} from '../types/project.interface'; 


export interface ITaskInterface {
    id: string
    name: string
    description: string
    project :IProjectInterface
  }